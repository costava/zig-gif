.PHONY: clean
clean:
	rm -rf zig-cache
	rm -rf zig-out
	rm -f out.gif
