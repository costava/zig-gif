# GIF Encoder

GIF ([GIF89a](https://www.w3.org/Graphics/GIF/spec-gif89a.txt)) encoding library written in [Zig](https://ziglang.org)

- Supports animation and looping (specifically [Netscape Looping Application Extension](http://www.vurdalakov.net/misc/gif/netscape-looping-application-extension)).
- Frames are diffed automatically so that only the bounding rectangle where changes occur is updated.
- Each frame can be passed either as a buffer of color indexes or a buffer of full RGB888 color (Red component comes first in buffer. Most similar color of palette will be used).
- A default color palette is used if a color palette is not passed in.

See `src/example.zig` for an example of usage.

Tested with Zig v0.11.0

A sample `out.gif` can be generated with `zig run src/example.zig`

## License

See file `LICENSE.txt`
