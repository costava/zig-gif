const std = @import("std");
const gif_encode = @import("gif_encode.zig");

pub fn main() !void {
    std.log.info("Hello.", .{});

    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer {
        if (gpa.deinit() == std.heap.Check.leak) {
            std.debug.print("****** LEAKED ******", .{});
        }
    }

    var dir: std.fs.Dir = std.fs.cwd();
    var file: std.fs.File = try dir.createFile("out.gif", .{
        .truncate = true,
    });
    defer file.close();
    var writer = file.writer();

    ////////////////////////////////////////////////////////////////////////////

    var gif = gif_encode.Gif(std.fs.File.Writer){
        .allocator = &gpa.allocator(),
        .writer = &writer,
    };

    const comment = "Custom text 123456";
    const width = 400;
    const height = 200;
    try gif.begin(gif_encode.Options{
        .width = width,
        .height = height,
        .num_iterations = 0,
        .palette_exponent = 7,
        .comment = comment,
        .comment_length = comment.len,
    });

    // 3 solid color frames: green, blue, black.

    {
        var buf = [_]u8{ 0, 255, 0 } ** (width * height);
        try gif.addFrame(&buf, 500);

        buf = [_]u8{ 0, 0, 255 } ** (width * height);
        try gif.addFrame(&buf, 500);
    }

    {
        // Black is 0th index of the default color palette.
        var buf = [_]u8{0} ** (width * height);
        try gif.addFramePalette(&buf, 500);
    }

    try gif.end();
}
