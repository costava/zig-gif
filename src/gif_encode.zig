//
// gif_encode
//
// Synopsis: GIF (GIF89a) encoding library
// Repo: https://codeberg.org/costava/zig-gif
// License:
//
// BSD 2-Clause License
//
// Copyright 2024 costava
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
// ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

const std = @import("std");

/// Return distance between 2 colors a and b
pub fn colorDifference(a: *[3]u8, b: *[3]u8) f32 {
    const r1: f32 = @as(f32, @floatFromInt(a[0]));
    const g1: f32 = @as(f32, @floatFromInt(a[1]));
    const b1: f32 = @as(f32, @floatFromInt(a[2]));

    const r2: f32 = @as(f32, @floatFromInt(b[0]));
    const g2: f32 = @as(f32, @floatFromInt(b[1]));
    const b2: f32 = @as(f32, @floatFromInt(b[2]));

    const dr = r2 - r1;
    const dg = g2 - g1;
    const db = b2 - b1;

    return @sqrt(dr * dr + dg * dg + db * db);
}

/// Return index of closest color in palette to given color
pub fn closestColorIndex(color: *[3]u8, palette: *[256 * 3]u8, num_colors: u16) u8 {
    var index: u8 = 0;
    var difference: f32 = colorDifference(color, palette[0..3]);

    var i: u16 = 1;
    while (i < num_colors) : (i += 1) {
        const new_difference = colorDifference(color, palette[(i * 3)..][0..3]);
        if (new_difference < difference) {
            index = @as(u8, @truncate(i));
            difference = new_difference;
        }
    }

    return index;
}

pub const Options = struct {
    // Width and height of the GIF image in pixels.
    width: u16,
    height: u16,

    // null to exclude looping information.
    // 0 to loop infinitely
    // Else the number of iterations wanted.
    num_iterations: ?u16,

    // Palette will have 2^(palette_exponent + 1) colors.
    palette_exponent: u3,

    // Pointer to 2^(palette_exponent + 1) RGB colors
    // where each color channel is 8 bits.
    // Optional because you can instead provide a sample_frame to get an
    // auto-generated palette or provide neither and get the default palette.
    palette: ?[*]u8 = null,

    // Provide a pointer to a sample frame to get
    // an auto-generated palette from the frame.
    // Ignored if palette is provided.
    sample_frame: ?[*]u8 = null,

    // An optional ASCII string if want a comment in the GIF.
    // null terminator not required.
    comment: ?[*]const u8 = null,

    // The number of bytes in comment.
    // Comment will not be written if comment_length not also provided.
    comment_length: ?u32 = null,
};

// Progress/state of outputting the gif file.
// The state is tracked in order to prevent an invalid order of operations.
const State = enum {
    initialized,
    adding_frames,
    finished,
};

// Errors specific to this library.
const Error = error{
    NotInInitializedState,
    NotInAddingFramesState,
};

pub fn Gif(comptime WriterType: type) type {
    return struct {
        allocator: *const std.mem.Allocator,
        writer: *WriterType,
        width: u16 = undefined,
        height: u16 = undefined,
        palette: [256 * 3]u8 = undefined, // Able to fit largest possible.
        num_colors: u16 = undefined,
        palette_exponent: u3 = undefined,
        state: State = State.initialized,
        // Previous frame as an array of palette color indexes.
        prev_frame_palette: []u8 = undefined,
        frame_count: u32 = 0,

        pub fn begin(self: *@This(), opt: Options) !void {
            if (self.state != State.initialized) {
                return Error.NotInInitializedState;
            }

            self.width = opt.width;
            self.height = opt.height;
            self.palette_exponent = opt.palette_exponent;
            self.num_colors = @as(u16, 2) << opt.palette_exponent;
            self.prev_frame_palette = try self.allocator.alloc(u8, @as(u32, self.width) * self.height);

            if (opt.palette) |palette| {
                var i: u16 = 0;
                while (i < self.num_colors * 3) {
                    self.palette[i] = palette[i];
                    i += 1;
                }
            } else if (opt.sample_frame) |sample_frame| {
                // TODO: Generate palette from sample_frame.
                _ = sample_frame;
                std.os.exit(1);
            } else {
                // Use default palette.

                var i: u16 = 0;
                while (i < self.num_colors * 3) {
                    self.palette[i] = DefaultPalette[i];
                    i += 1;
                }
            }

            // Write Header
            try self.writer.writeAll("GIF89a");

            // Write Logical Screen Descriptor

            try self.writer.writeByte(@as(u8, @truncate(self.width)));
            try self.writer.writeByte(@as(u8, @truncate(self.width >> 8)));
            try self.writer.writeByte(@as(u8, @truncate(self.height)));
            try self.writer.writeByte(@as(u8, @truncate(self.height >> 8)));

            const lsdBitField = LsdBitField{
                .global_color_table_size = opt.palette_exponent,
                .sort_flag = 0, // GCT is not ordered by importance.
                .color_resolution = 7, // Each color channel is 8 bits.
                .global_color_table_flag = 1, // GCT exists.
            };

            const lsdBitFieldByte: u8 = @as(u8, @bitCast(lsdBitField));

            try self.writer.writeByte(lsdBitFieldByte);

            // Write 0 for Background Color Index.
            // We will never see the background.
            try self.writer.writeByte(0);

            // Set Pixel Aspect Ratio for square pixels.
            try self.writer.writeByte(49);

            // Write Global Color Table
            {
                var i: u16 = 0;
                while (i < self.num_colors * 3) {
                    try self.writer.writeByte(self.palette[i]);
                    i += 1;
                }
            }

            // Write loop extension if applicable.
            // http://www.vurdalakov.net/misc/gif/netscape-looping-application-extension

            if (opt.num_iterations) |num_iterations| {
                // Extension Label
                try self.writer.writeByte(0x21);
                // Application Extension Label
                try self.writer.writeByte(0xFF);
                // Block Size (fixed)
                try self.writer.writeByte(0x0B);
                // Application Identifier
                try self.writer.writeAll("NETSCAPE");
                // Application Authentication Code
                try self.writer.writeAll("2.0");
                // Sub-block Data Size
                try self.writer.writeByte(0x03);
                // Sub-block ID
                try self.writer.writeByte(0x01);
                // Loop Count
                try self.writer.writeByte(@as(u8, @truncate(num_iterations)));
                try self.writer.writeByte(@as(u8, @truncate(num_iterations >> 8)));
                // Block Terminator
                try self.writer.writeByte(0x00);
            }

            // Write comment extension if applicable.

            if (opt.comment) |comment| {
                if (opt.comment_length) |comment_length| {
                    // Extension Introducer
                    try self.writer.writeByte(0x21);
                    // Comment Label
                    try self.writer.writeByte(0xFE);

                    try self.writeSubBlocks(comment, comment_length);
                }
            }

            self.state = State.adding_frames;
        }

        pub fn addFrame(self: *@This(), buf: []u8, delay_time_ms: u16) !void {
            if (self.state != State.adding_frames) {
                return Error.NotInAddingFramesState;
            }

            // Convert frame to palette first.

            var pal_buf = try self.allocator.alloc(u8, @as(u32, self.width) * self.height);
            defer self.allocator.free(pal_buf);

            var p: u32 = 0;
            while (p < @as(u32, self.width) * self.height) : (p += 1) {
                pal_buf[p] = closestColorIndex(buf[(p * 3)..][0..3], &self.palette, self.num_colors);
            }

            try self.addFramePalette(pal_buf, delay_time_ms);
        }

        pub fn addFramePalette(self: *@This(), buf: []u8, delay_time_ms: u16) !void {
            if (self.state != State.adding_frames) {
                return Error.NotInAddingFramesState;
            }

            // Write Graphic Control Extension

            // Extension Introducer
            try self.writer.writeByte(0x21);
            // Graphic Control Label
            try self.writer.writeByte(0xf9);
            // Block Size (fixed)
            try self.writer.writeByte(4);

            // Packed Fields
            const gceBitField = GceBitField{
                .transparent_color_flag = 0, // No transparent index given.
                .user_input_flag = 0, // No user input expected before continuing.
                .disposal_method = 1, // Leave in place.
            };

            try self.writer.writeByte(@as(u8, @bitCast(gceBitField)));

            // Convert to hundredths of a second (centisecond).
            const delay_time_cs: u16 = @as(u16, @intFromFloat(@round(@as(f32, @floatFromInt(delay_time_ms)) / 10.0)));

            // Delay Time
            try self.writer.writeByte(@as(u8, @truncate(delay_time_cs)));
            try self.writer.writeByte(@as(u8, @truncate(delay_time_cs >> 8)));

            // Transparent Color Index. Present but ignored since TC flag is 0.
            try self.writer.writeByte(255);

            // Block Terminator
            try self.writer.writeByte(0x00);

            // Calculate position and dimensions of image to add.
            var left_pos: u16 = undefined;
            var top_pos: u16 = undefined;
            var image_width: u16 = undefined;
            var image_height: u16 = undefined;

            if (self.frame_count == 0) {
                // First frame. Update entire image.
                // TODO: It is maybe possible to take advantage of background color
                //       and not have to update entire image?
                left_pos = 0;
                top_pos = 0;
                image_width = self.width;
                image_height = self.height;
            } else {
                // Find first difference.
                var first_diff_i: ?u32 = null;
                var i: u32 = 0;
                while (i < @as(u32, self.width) * self.height) : (i += 1) {
                    if (self.prev_frame_palette[i] != buf[i]) {
                        first_diff_i = i;
                        break;
                    }
                }

                if (first_diff_i) |fdi| {
                    var leftmost: u16 = @as(u16, @truncate(fdi % self.width));
                    var rightmost: u16 = leftmost;
                    var topmost: u16 = @as(u16, @truncate(fdi / self.width));
                    var bottommost: u16 = topmost;

                    var j: u32 = fdi;
                    while (j < @as(u32, self.width) * self.height) : (j += 1) {
                        const x = @as(u16, @truncate(j % self.width));
                        const y = @as(u16, @truncate(j / self.width));

                        if (x > rightmost) {
                            rightmost = x;
                        }

                        if (y > bottommost) {
                            bottommost = y;
                        }
                    }

                    left_pos = leftmost;
                    top_pos = topmost;
                    image_width = rightmost - leftmost + 1;
                    image_height = bottommost - topmost + 1;
                } else {
                    // No difference. Update entire frame.
                    // TODO: Can we set width and height to 0 instead?
                    left_pos = 0;
                    top_pos = 0;
                    image_width = self.width;
                    image_height = self.height;
                }
            }

            // Write Image Descriptor

            // Image Separator
            try self.writer.writeByte(0x2c);

            // Image Left Position
            try self.writer.writeByte(@as(u8, @truncate(left_pos)));
            try self.writer.writeByte(@as(u8, @truncate(left_pos >> 8)));

            // Image Top Position
            try self.writer.writeByte(@as(u8, @truncate(top_pos)));
            try self.writer.writeByte(@as(u8, @truncate(top_pos >> 8)));

            // Image Width
            try self.writer.writeByte(@as(u8, @truncate(image_width)));
            try self.writer.writeByte(@as(u8, @truncate(image_width >> 8)));

            // Image Height
            try self.writer.writeByte(@as(u8, @truncate(image_height)));
            try self.writer.writeByte(@as(u8, @truncate(image_height >> 8)));

            // Packed Fields
            const idBitField = IdBitField{
                .local_color_table_flag = 0, // No local color table.
                .interlace_flag = 0, // Not interlaced.
                .sort_flag = 0, // Local color table is not ordered (does not even exist).
                .size_of_local_color_table = 0, // No local color table.
            };

            const idBitFieldByte: u8 = @as(u8, @bitCast(idBitField));
            try self.writer.writeByte(idBitFieldByte);

            // Write image data

            // Get the data we need into one straight buffer.
            var serial_buf: []u8 = try self.allocator.alloc(u8, @as(u32, image_width) * image_height);
            defer self.allocator.free(serial_buf);

            {
                // Use this index as we fill serial_buf
                var sbufi: u32 = 0;

                var y: u32 = top_pos;
                while (y < top_pos + image_height) : (y += 1) {
                    var x: u32 = left_pos;
                    while (x < left_pos + image_width) : (x += 1) {
                        var k: u32 = y * self.width + x;

                        serial_buf[sbufi] = buf[k];
                        sbufi += 1;
                    }
                }
            }

            {
                const lzw_min_code_size: u8 = @as(u8, self.palette_exponent) + 1;
                try self.writer.writeByte(lzw_min_code_size);

                var code_writer = LzwCodeWriter(WriterType){
                    .writer = self.writer,
                    .lzw_min_code_size = @as(u4, @truncate(lzw_min_code_size)),
                };

                try code_writer.init(self.allocator);

                for (serial_buf) |value| {
                    try code_writer.addCode(value);
                }

                try code_writer.end();
            }

            try self.writer.writeByte(0); // 0 len block

            // Update prev_frame_palette
            {
                var k: u32 = 0;
                while (k < @as(u32, self.width) * self.height) : (k += 1) {
                    self.prev_frame_palette[k] = buf[k];
                }
            }

            // Increment frame count
            self.frame_count += 1;
        }

        pub fn end(self: *@This()) !void {
            if (self.state != State.adding_frames) {
                return Error.NotInAddingFramesState;
            }

            // Trailer
            try self.writer.writeByte(0x3B);

            self.state = State.finished;

            self.allocator.free(self.prev_frame_palette);
        }

        fn writeSubBlocks(self: *@This(), data: [*]const u8, dataLength: u32) !void {
            var remaining: u32 = dataLength;
            var current: [*]const u8 = data;

            while (remaining > 0) {
                const blockSize: u8 = if (remaining >= 255) 255 else @as(u8, @truncate(remaining));
                try self.writer.writeByte(blockSize);

                {
                    var i: u16 = 0;
                    while (i < blockSize) {
                        try self.writer.writeByte(current[0]);
                        current += 1;
                        i += 1;
                    }
                }

                remaining -= blockSize;
            }

            // Block Terminator
            try self.writer.writeByte(0x00);
        }
    };
}

const LzwCode = struct {
    value: u12,
    num_bits: u4,
};

// https://en.wikipedia.org/wiki/Lempel%E2%80%93Ziv%E2%80%93Welch#Encoding
// Has a good overview of LZW encoding:
// 1. Initialize the dictionary to contain all strings of length one.
// 2. Find the longest string W in the dictionary that matches the current input.
// 3. Emit the dictionary index for W to output and remove W from the input.
// 4. Add W followed by the next symbol in the input to the dictionary.
// 5. Go to Step 2.

// var sum = 0;
// for (var i = 1; i <= 4096; i += 1) {sum += i;}
// console.log(sum);
//
// Sums to 8390656
// This is more than enough.

const LCW_STRINGS_CAPACITY = 8390656;

// Handles the building and writing of 8-bit bytes given LZW codes.
pub fn LzwCodeWriter(comptime WriterType: type) type {
    return struct {
        writer: *WriterType,
        allocator: *const std.mem.Allocator = undefined,
        lzw_min_code_size: u4,
        lzw_current_code_size: u4 = undefined,

        current: u8 = 0,
        num_bits: u4 = 0, // Number of bits stored in current.
        buf: [255]u8 = undefined,
        buf_len: u8 = 0,

        currentString: [4096]u16 = undefined,
        currentStringLength: u16 = undefined,

        // Must be dynamically allocated so as to not overflow stack.
        strings: *[LCW_STRINGS_CAPACITY]u16 = undefined, // Buffer to hold all string data of table.
        stringsLength: u32 = undefined, // How many units long is `strings`
        stringStarts: [4096]u32 = undefined, // Where the nth string begins.
        stringLengths: [4096]u16 = undefined, // How many codes long is the nth string.
        numStrings: u16 = undefined, // How many strings are in the table.

        pub fn init(self: *@This(), allocator: *const std.mem.Allocator) !void {
            self.allocator = allocator;
            self.strings = try self.allocator.*.create([LCW_STRINGS_CAPACITY]u16);

            self.resetTable();
            self.currentStringLength = 0;

            {
                // "Encoders should output a Clear code as the first code of each image data stream."
                //    ~page 31 of spec.

                // Do NOT do this. The generated gif does not work correctly in gwenview.

                // const clearCode = std.math.pow(u16, 2, self.lzw_min_code_size);
                // try self.addCode(clearCode);
            }
        }

        // If the table has space, add the given string to it.
        fn addStringToTableIfCan(self: *@This(), str: [4096]u16, strLength: u16) void {
            if (self.numStrings == 4096) {
                return;
            }

            std.debug.assert(self.numStrings < 4096);

            std.debug.assert((self.stringsLength + strLength) <= LCW_STRINGS_CAPACITY);

            self.stringStarts[self.numStrings] = self.stringsLength;
            self.stringLengths[self.numStrings] = strLength;

            var c: u32 = 0;
            while (c < strLength) : (c += 1) {
                self.strings[self.stringStarts[self.numStrings] + c] = str[c];
            }

            self.numStrings += 1;
            self.stringsLength += strLength;

            // Check if lzw_current_code_size be incremented.

            std.debug.assert(self.lzw_current_code_size < 12);

            const numIndexesAvailable = std.math.pow(u16, 2, self.lzw_current_code_size);

            if (self.numStrings > numIndexesAvailable) {
                self.lzw_current_code_size += 1;
            }
        }

        // Return index of givenstring in table if possible.
        fn findString(self: *@This(), str: [4096]u16, strLength: u16) ?u16 {
            std.debug.assert(strLength > 0);

            var i: u16 = 0;
            while (i < self.numStrings) : (i += 1) {
                if (self.stringLengths[i] != strLength) {
                    continue;
                }

                var matching: bool = true;
                var c: u16 = 0;
                while (c < strLength) {
                    if (str[c] != self.strings[self.stringStarts[i] + c]) {
                        matching = false;
                        break;
                    }

                    c += 1;
                }

                if (matching) return i;
            }

            return null;
        }

        fn resetTable(self: *@This()) void {
            // "This code size value also implies that the compression codes must start out one bit longer."
            //    ~page 31 of spec.
            self.lzw_current_code_size = self.lzw_min_code_size + 1;

            self.numStrings = std.math.pow(u16, 2, self.lzw_min_code_size);
            self.stringsLength = 0;

            {
                var i: u16 = 0;
                while (i < self.numStrings) : (i += 1) {
                    self.strings[i] = i;
                    self.stringStarts[i] = i;
                    self.stringLengths[i] = 1;
                }
            }

            self.stringsLength = self.numStrings;

            // Add clear code
            self.strings[self.stringsLength] = self.numStrings;
            self.stringStarts[self.numStrings] = self.stringsLength;
            self.stringLengths[self.numStrings] = 1;
            self.stringsLength += 1;
            self.numStrings += 1;

            // Add end code
            self.strings[self.stringsLength] = self.numStrings;
            self.stringStarts[self.numStrings] = self.stringsLength;
            self.stringLengths[self.numStrings] = 1;
            self.stringsLength += 1;
            self.numStrings += 1;
        }

        fn writeOut(self: *@This()) !void {
            if (self.buf_len == 0) {
                return;
            }

            // We are writing Data Sub-Blocks
            // First byte is length in bytes of sub-block
            // (length does not include the length byte).
            try self.writer.writeByte(self.buf_len);

            var i: u16 = 0;
            while (i < self.buf_len) : (i += 1) {
                try self.writer.writeByte(self.buf[i]);
            }

            self.buf_len = 0;

            // Do not clear `current` nor `num_bits`.
        }

        fn writeOutIfFull(self: *@This()) !void {
            if (self.buf_len == 255) {
                try self.writeOut();

                std.debug.assert(self.buf_len == 0);
            }
        }

        fn writeCurrent(self: *@This()) !void {
            std.debug.assert(self.num_bits > 0);

            try self.writeOutIfFull();

            std.debug.assert(self.buf_len < 255);

            self.buf[self.buf_len] = self.current;
            self.buf_len += 1;

            self.current = 0;
            self.num_bits = 0;
        }

        pub fn addCode(self: *@This(), value: u16) std.os.WriteError!void {
            std.debug.assert(self.currentStringLength < 4096);

            self.currentString[self.currentStringLength] = value;
            self.currentStringLength += 1;

            const maybeStringIdx = self.findString(self.currentString, self.currentStringLength);

            if (maybeStringIdx) |_| {
                return;
            }

            const maybeWIdx = self.findString(self.currentString, self.currentStringLength - 1);

            if (maybeWIdx) |wIdx| {
                try self.outputCode(wIdx, self.lzw_current_code_size);

                self.addStringToTableIfCan(self.currentString, self.currentStringLength);

                self.currentString[0] = self.currentString[self.currentStringLength - 1];
                self.currentStringLength = 1;
            } else {
                // Should be unreachable.

                std.debug.print("Substring should always exist in table.\n", .{});
                std.debug.print("self.numStrings: {}\n", .{self.numStrings});
                std.debug.print("self.currentStringLength: {}\n", .{self.currentStringLength});
                std.debug.print("self.currentString:\n", .{});
                {
                    var i: u16 = 0;
                    while (i < self.currentStringLength) : (i += 1) {
                        std.debug.print("\t[{}]: {}\n", .{ i, self.currentString[i] });
                    }
                }
                std.process.exit(1);
            }
        }

        fn outputCode(self: *@This(), value: u16, num_bits: u4) std.os.WriteError!void {
            var num_bits_remaining = num_bits;
            var remaining = value;

            std.debug.assert(num_bits_remaining > 0);

            while (num_bits_remaining > 0) {
                std.debug.assert(self.num_bits < 8);
                const max_bits_used = 8 - self.num_bits;
                const bits_used = if (num_bits_remaining >= max_bits_used) max_bits_used else num_bits_remaining;

                self.current = self.current | @as(u8, @truncate(remaining << self.num_bits));
                self.num_bits += bits_used;
                num_bits_remaining -= bits_used;
                remaining = remaining >> bits_used;

                std.debug.assert(self.num_bits <= 8);

                if (self.num_bits == 8) {
                    try self.writeCurrent();
                }
            }

            std.debug.assert(remaining == 0);
        }

        fn writeRemainingCurrentString(self: *@This()) !void {
            var tryLength = self.currentStringLength;

            while (self.currentStringLength > 0) {
                const maybeStringIdx = self.findString(self.currentString, tryLength);

                if (maybeStringIdx) |stringIdx| {
                    try self.outputCode(stringIdx, self.lzw_current_code_size);

                    if (tryLength == self.currentStringLength) {
                        self.currentStringLength = 0;
                        return;
                    } else {
                        std.debug.assert(tryLength < self.currentStringLength);

                        // Recall that this can change the current code size.
                        // So we shouldn't skip it even if we don't expect to ever use that table entry.
                        self.addStringToTableIfCan(self.currentString, tryLength + 1);

                        const numRemaining = self.currentStringLength - tryLength;
                        var r: u16 = 0;
                        while (r < numRemaining) : (r += 1) {
                            self.currentString[r] = self.currentString[tryLength + r];
                        }

                        self.currentStringLength = numRemaining;
                        tryLength = self.currentStringLength;
                    }
                } else {
                    std.debug.assert(tryLength > 1);

                    tryLength -= 1;
                }
            }
        }

        // Must be called at end to write out any remaining buffered content.
        pub fn end(self: *@This()) !void {
            try self.writeRemainingCurrentString();

            {
                const end_of_information_code = std.math.pow(u16, 2, self.lzw_min_code_size) + 1;
                try self.outputCode(end_of_information_code, self.lzw_current_code_size);
            }

            if (self.num_bits > 0) {
                try self.writeCurrent();
            }

            std.debug.assert(self.num_bits == 0);

            try self.writeOut();

            std.debug.assert(self.buf_len == 0);

            self.allocator.*.free(self.strings);
        }
    };
}

// The 8-bit packed field in the Logical Screen Descriptor.
const LsdBitField = packed struct {
    global_color_table_size: u3,
    sort_flag: u1,
    color_resolution: u3,
    global_color_table_flag: u1,
};

// The 8-bit packed field in the Graphic Control Extension.
const GceBitField = packed struct {
    transparent_color_flag: u1,
    user_input_flag: u1,
    disposal_method: u3,
    reserved: u3 = 0,
};

// The 8-bit packed field in the Image Descriptor.
const IdBitField = packed struct {
    size_of_local_color_table: u3,
    reserved: u2 = 0,
    sort_flag: u1,
    interlace_flag: u1,
    local_color_table_flag: u1,
};

// Color channel order is RGB (R first).
const DefaultPalette = [256 * 3]u8{
    0,   0,   0,
    255, 255, 255,
    0,   255, 0,
    0,   0,   255,
    255, 0,   0,
    255, 255, 0,
    255, 0,   255,
    0,   255, 255,
    215, 39,  42,
    46,  181, 70,
    174, 239, 120,
    123, 173, 143,
    76,  102, 102,
    73,  56,  103,
    155, 171, 3,
    153, 80,  107,
    234, 90,  153,
    16,  176, 103,
    214, 136, 142,
    71,  101, 51,
    191, 224, 224,
    181, 117, 127,
    28,  16,  42,
    138, 147, 212,
    139, 122, 13,
    26,  123, 195,
    93,  194, 40,
    183, 132, 171,
    54,  161, 187,
    96,  192, 101,
    218, 75,  248,
    51,  250, 22,
    64,  20,  145,
    4,   114, 83,
    44,  2,   212,
    53,  115, 163,
    107, 42,  40,
    209, 192, 137,
    47,  72,  67,
    203, 124, 62,
    225, 188, 82,
    114, 192, 196,
    153, 34,  58,
    60,  141, 100,
    139, 218, 102,
    207, 165, 226,
    13,  134, 159,
    95,  248, 95,
    235, 86,  111,
    37,  147, 252,
    137, 247, 160,
    56,  67,  199,
    54,  199, 125,
    100, 109, 198,
    255, 194, 253,
    63,  250, 64,
    6,   209, 227,
    198, 8,   170,
    125, 107, 147,
    219, 250, 64,
    94,  157, 190,
    189, 1,   43,
    131, 0,   238,
    128, 64,  232,
    193, 70,  186,
    164, 12,  194,
    171, 102, 202,
    139, 221, 15,
    115, 158, 85,
    239, 145, 177,
    60,  61,  23,
    7,   80,  10,
    78,  168, 150,
    24,  86,  130,
    237, 142, 53,
    154, 149, 133,
    139, 80,  174,
    16,  139, 42,
    216, 39,  150,
    185, 83,  45,
    85,  11,  13,
    4,   27,  152,
    46,  243, 191,
    174, 202, 186,
    201, 25,  96,
    32,  12,  120,
    85,  14,  99,
    101, 253, 200,
    46,  101, 9,
    232, 100, 201,
    102, 233, 247,
    245, 38,  86,
    189, 214, 26,
    226, 83,  53,
    125, 51,  89,
    42,  28,  178,
    18,  87,  227,
    160, 200, 50,
    18,  218, 137,
    50,  0,   253,
    29,  192, 184,
    65,  254, 247,
    68,  89,  242,
    251, 187, 170,
    14,  45,  202,
    116, 133, 49,
    108, 246, 133,
    133, 35,  1,
    255, 188, 28,
    239, 1,   108,
    134, 118, 243,
    189, 150, 19,
    92,  217, 162,
    41,  236, 103,
    231, 234, 194,
    80,  33,  212,
    100, 68,  182,
    2,   148, 3,
    191, 68,  112,
    250, 105, 8,
    244, 42,  4,
    139, 183, 249,
    117, 27,  179,
    14,  232, 58,
    232, 25,  186,
    134, 70,  55,
    238, 251, 119,
    250, 119, 249,
    181, 35,  19,
    145, 6,   153,
    139, 226, 235,
    2,   70,  67,
    15,  195, 9,
    183, 59,  227,
    163, 157, 67,
    132, 84,  1,
    86,  62,  141,
    52,  203, 236,
    195, 170, 171,
    5,   31,  86,
    97,  251, 31,
    75,  221, 0,
    214, 80,  2,
    250, 155, 223,
    251, 61,  207,
    218, 137, 98,
    121, 110, 88,
    249, 187, 125,
    117, 158, 2,
    81,  145, 241,
    179, 103, 9,
    36,  254, 152,
    253, 147, 7,
    237, 33,  235,
    173, 188, 108,
    21,  97,  45,
    147, 1,   27,
    193, 223, 156,
    236, 0,   151,
    131, 1,   71,
    203, 184, 4,
    177, 117, 243,
    192, 12,  231,
    214, 243, 7,
    92,  131, 149,
    68,  17,  62,
    172, 241, 51,
    102, 14,  136,
    2,   172, 249,
    159, 117, 75,
    42,  159, 17,
    146, 18,  109,
    30,  77,  169,
    254, 138, 132,
    250, 109, 82,
    215, 255, 245,
    71,  131, 20,
    141, 52,  137,
    10,  174, 154,
    32,  39,  244,
    6,   251, 204,
    142, 253, 77,
    123, 215, 56,
    166, 251, 251,
    255, 52,  165,
    206, 178, 43,
    0,   224, 174,
    77,  179, 0,
    9,   118, 253,
    178, 81,  149,
    24,  41,  4,
    79,  155, 67,
    233, 218, 44,
    176, 255, 178,
    248, 1,   213,
    71,  42,  252,
    78,  213, 206,
    146, 211, 145,
    223, 201, 219,
    13,  0,   181,
    4,   156, 208,
    240, 1,   55,
    47,  210, 38,
    65,  121, 212,
    68,  255, 125,
    158, 45,  255,
    94,  192, 252,
    186, 192, 248,
    1,   179, 66,
    251, 249, 163,
    95,  81,  24,
    161, 254, 14,
    96,  73,  69,
    68,  213, 79,
    201, 219, 90,
    3,   130, 120,
    1,   250, 110,
    90,  1,   252,
    215, 3,   4,
    128, 146, 172,
    80,  1,   180,
    137, 224, 193,
    184, 3,   133,
    46,  1,   5,
    252, 41,  127,
    38,  49,  135,
    253, 222, 95,
    255, 150, 90,
    162, 46,  173,
    215, 137, 254,
    36,  151, 130,
    169, 156, 243,
    129, 171, 44,
    51,  218, 160,
    172, 2,   82,
    37,  0,   80,
    253, 51,  47,
    202, 129, 210,
    110, 80,  114,
    188, 54,  72,
    251, 255, 40,
    92,  49,  1,
    2,   213, 96,
    215, 119, 20,
    76,  254, 167,
    173, 166, 204,
    157, 85,  255,
    0,   53,  120,
    254, 226, 226,
    243, 217, 0,
    106, 90,  255,
    112, 34,  255,
    154, 255, 212,
    44,  111, 79,
    0,   45,  36,
    33,  238, 230,
};
